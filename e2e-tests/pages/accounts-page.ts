import { Selector } from 'testcafe';

export class AccountsPage {
    public accountsLabel: Selector;
    public accountsMenu: Selector;

    constructor() {
        this.accountsLabel = Selector('.page-title').find('span').withText('Accounts')
        this.accountsMenu = Selector('[id="btn-accounts"]');
    }
}
