import { Selector } from 'testcafe';

export class LandingPage {
    //Authentication
    public username: Selector;
    public password: Selector;
    public submit: Selector;

    constructor() {
        this.username = Selector('[id="username"]')
        this.password = Selector('[id="password"]')
        this.submit = Selector('[id="kc-login"]');
    }

}
