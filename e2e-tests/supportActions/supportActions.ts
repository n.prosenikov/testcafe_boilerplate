// // Deep Equal
// .expect(Selector('.titleText').innerText).eql("My Page for (Customer Code:000MIS）")

// // Not Equal
// .expect(Selector('a').withText('Login').textContent).notEql("Login Test")

// // Is OK
// .expect(Selector('#keyword_go').value).ok()

// // Not OK
// .expect(Selector('#keyword_go').value).notOk()

// // Click
// .click(Selector('a').withText('Login'))

// // Right Click
// .rightClick(Selector('span').withText('Test'))

// // Press Key
// .pressKey('ctrl+v')
// .pressKey('enter')
// .pressKey('enter', {
//     speed: 1
// })

// // Find By Specific text
// .click(Selector('a').withText('close'))

// // If Contains
// .expect(Selector('#keyword_go').value).contains("Search")

// // Not Contains
// .expect(Selector('#keyword_go').value).notContains('')

// // Type
// .expect(Selector('#keyword_go').value).typeOf('')

// // Greater
// .expect(Selector('#keyword_go').value).gt(10)

// // Within
// .expect(Selector('#keyword_go').value).within(1)

// // Drag and Drop
// .drag(Selector('#keyword_go'), 111, 222)

// // Drag to Element
// .dragToElement(Selector('#keyword_go'), Selector('[data-common-userlink="register"]'))

// // Select text
// .selectText(Selector('#keyword_go'), 1, 2)

// // Select text area content
// .selectTextAreaContent(Selector('li').withText('Scheduled Maintenance Notice'), 1, 1, 2, 2)

// // Hover
// .hover(Selector('a').withText('Automation Components'))

// // FIles to Upload
// .setFilesToUpload(Selector('a').withText('Automation Components'), [])

// // Handle native dialogs
// .setNativeDialogHandler((dialogType, message, url) => {
//     if (dialogType === 'confirm')
//         return true;

//     throw Error('An unexpected ' + dialogType + ' dialog with the message "' + message + '" appeared on the page ' + url);
// })
 
// // Screenshot
// .takeScreenshot('/screenshots')

// // Switch to IFrame
// .switchToIframe(Selector());

// });

// // Record video
// testcafe chrome e2e-tests/features/01_loginFixture.ts --video e2e-tests/videos