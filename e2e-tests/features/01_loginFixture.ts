import { environment } from './../../env/environment';
import { LandingPage } from '../pages/landing-page';
import { AccountsPage } from '../pages/accounts-page';

const landingPage: LandingPage = new LandingPage();
const accountsPage: AccountsPage = new AccountsPage();

fixture`Login`
    .page`http://rowius.s1.blubito.com/#/`

test('Login boilerplate', async t => {
    await t
        .setTestSpeed(0.7)
        .maximizeWindow()
        .typeText(landingPage.username, environment.username)
        .typeText(landingPage.password, environment.password, {
            caretPos: 0
        })
        .click(landingPage.submit)
        .click(accountsPage.accountsMenu)
        .expect(accountsPage.accountsLabel.textContent).eql("Accounts");
        
});